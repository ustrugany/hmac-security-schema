<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request;


interface RequestInterface
{
    /**
     * @return string
     */
    public function getPublicKey();

    /**
     * @return []
     */
    public function getParameters();

    /**
     * @param string $name
     * @param $value
     * @return $this
     */
    public function addParameter($name, $value);

    /**
     * @return string
     */
    public function getSignature();

    /**
     * @param string $signature
     * @return $this
     */
    public function setSignature($signature);

    /**
     * @return string
     */
    public function getSignedQueryString();
}