<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request;


interface RequestSignerInterface
{
    /**
     * @param RequestInterface $request
     * @param string $secret
     * @return RequestInterface
     */
    public function sign(RequestInterface $request, $secret);
}