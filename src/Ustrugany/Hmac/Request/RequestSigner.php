<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request;


use Ustrugany\Hmac\Signature\SignatureGenerator;

class RequestSigner implements RequestSignerInterface
{
    /**
     * @param RequestInterface $request
     * @return RequestInterface
     */
    public function sign(RequestInterface $request, $secret)
    {
        $parameters = $request->getParameters();
        $signature = (new SignatureGenerator())->generate($parameters, $secret);
        $request->setSignature($signature);

        return $request;
    }
}