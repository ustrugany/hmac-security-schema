<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request\Parameters;

interface ParametersSorterInterface
{
    /**
     * @param [] $parameters
     * @return []
     */
    public function sort(array $parameters);
}