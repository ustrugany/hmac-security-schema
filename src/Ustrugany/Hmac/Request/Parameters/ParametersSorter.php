<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request\Parameters;


/**
 * Class ParametersSorter
 * @package Ustrugany\Hmac\Request\Parameters
 */
class ParametersSorter implements ParametersSorterInterface
{
    /**
     * @param [] $parameters
     * @return []
     */
    public function sort(array $parameters)
    {
        ksort($parameters);

        return $parameters;
    }
}