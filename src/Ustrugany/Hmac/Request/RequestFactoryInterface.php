<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request;


interface RequestFactoryInterface
{
    /**
     * @param array $parameters
     * @param string $publicKey
     * @return RequestInterface
     */
    public function create(array $parameters, $publicKey);
}