<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request;


class RequestFactory implements RequestFactoryInterface
{
    /**
     * @param array $parameters
     * @param string $publicKey
     * @return RequestInterface
     */
    public function create(array $parameters, $publicKey)
    {
        return (new Request($parameters, $publicKey));
    }
}