<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request;


class Request implements RequestInterface
{
    const TIMESTAMP_PARAMETER = 'timestamp';
    const PUBLIC_KEY_PARAMETER = 'public_key';
    const SIGNATURE_PARAMETER = 'signature';

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var string
     */
    private $signature;

    /**
     * @param array $parameters
     * @param string $publicKey
     */
    public function __construct(array $parameters, $publicKey)
    {
        $this->parameters = $parameters;
        $this->parameters[self::TIMESTAMP_PARAMETER] = (new \DateTime())->getTimestamp();
        $this->parameters[self::PUBLIC_KEY_PARAMETER] = $publicKey;
    }

    /**
     * @return string
     */
    public function getPublicKey()
    {
        return $this->parameters[self::PUBLIC_KEY_PARAMETER];
    }

    /**
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function addParameter($name, $value)
    {
        $this->parameters[$name] = $value;

        return $this;
    }

    /**
     * @return []
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * @return string
     */
    public function getSignedQueryString()
    {
        $parameters = $this->parameters;
        $parameters[self::SIGNATURE_PARAMETER] = $this->signature;

        return http_build_query($parameters);
    }
}