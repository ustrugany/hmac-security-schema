<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request\Validator;


use Ustrugany\Hmac\Request\Request;
use Ustrugany\Hmac\Signature\SignatureGenerator;
use Ustrugany\Hmac\Token\TokenRepositoryInterface;

abstract class RequestValidator implements ValidatorInterface
{
    /**
     * @var TokenRepositoryInterface
     */
    protected $tokenRepository;

    function __construct(TokenRepositoryInterface $tokenRepository)
    {
        $this->tokenRepository = $tokenRepository;
    }

    /**
     * @param array $parameters
     */
    public function validate(array $parameters)
    {
        $token = $this->tokenRepository->findByKey($parameters[Request::PUBLIC_KEY_PARAMETER]);
        $signature = $parameters[Request::SIGNATURE_PARAMETER];
        unset($parameters[Request::SIGNATURE_PARAMETER]);

        $checkSignature = (new SignatureGenerator())->generate($parameters, $token->getSecret());

        return $checkSignature == $signature;
    }
}