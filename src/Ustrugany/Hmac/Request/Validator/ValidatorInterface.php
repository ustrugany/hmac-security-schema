<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Request\Validator;

interface ValidatorInterface
{
    /**
     * @param array $parameters
     * @return boolean
     */
    public function validate(array $parameters);
}