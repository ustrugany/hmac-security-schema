<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Signature;


use Ustrugany\Hmac\Request\Parameters\ParametersSorter;

class SignatureGenerator implements SignatureGeneratorInterface
{
    const PARAMETERS_GLUE = '';
    const HASHING_ALGORITHM = 'sha256';

    /**
     * @param [] $parameters
     * @param string $secret
     * @return string
     */
    public function generate($parameters, $secret)
    {
        $parameters = (new ParametersSorter())->sort($parameters);
        $query = http_build_query($parameters);

        return base64_encode(hash_hmac(self::HASHING_ALGORITHMg, $query, $secret, true));
    }
}