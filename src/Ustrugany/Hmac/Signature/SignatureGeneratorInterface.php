<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Signature;

interface SignatureGeneratorInterface
{
    /**
     * @param [] $parameters
     * @param string $secret
     * @return string
     */
    public function generate($parameters, $secret);
}