<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\KeyGenerator;


interface KeyGeneratorInterface
{
    /**
     * @return string
     */
    public function generate();
}