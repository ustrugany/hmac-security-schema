<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\KeyGenerator;


class KeyGenerator implements KeyGeneratorInterface
{
    const HASHING_ALGORITHM = 'sha512';

    /**
     * @return string
     */
    public function generate()
    {
        $base = hash(self::HASHING_ALGORITHM, $this->getEntropy());

        if (function_exists('gmp_strval')) {
            return gmp_strval(gmp_init($base, 16), 62);
        }

        return base64_encode($base);
    }

    /**
     * @return string
     */
    private function getEntropy()
    {
        $fp = fopen('/dev/urandom', 'rb');
        $entropy = fread($fp, 32);
        fclose($fp);

        return $entropy.uniqid(mt_rand(), true);
    }
}