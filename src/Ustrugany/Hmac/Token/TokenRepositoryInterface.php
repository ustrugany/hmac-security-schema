<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Token;


interface TokenRepositoryInterface
{
    /**
     * @param string $key
     * @return TokenInterface|null
     */
    public function findByKey($key);
}