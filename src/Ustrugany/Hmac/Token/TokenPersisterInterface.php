<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Token;


interface TokenPersisterInterface
{
    /**
     * @param TokenInterface $token
     * @return TokenInterface
     */
    public function persist(TokenInterface $token);
}