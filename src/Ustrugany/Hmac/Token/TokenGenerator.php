<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Token;

use Ustrugany\Hmac\KeyGenerator\KeyGenerator;

class TokenGenerator extends KeyGenerator implements TokenGeneratorInterface
{
    /**
     * @return TokenInterface
     */
    public function generate()
    {
        $hash = parent::generate();

        return (new Token(substr($hash, 0, 32), substr($hash, 32, 48)));
    }
}