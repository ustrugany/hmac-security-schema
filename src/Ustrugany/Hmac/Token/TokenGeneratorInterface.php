<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Token;


interface TokenGeneratorInterface
{
    /**
     * @return TokenInterface
     */
    public function generate();
}