<?php

namespace Ustrugany\Hmac\Token;

class Token implements TokenInterface
{
    /**
     * @var string
     */
    protected $publicKey;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var \DateTime
     */
    protected $generatedAt;

    /**
     * @param string $publicKey
     * @param string $secret
     */
    public function __construct($publicKey, $secret)
    {
        $this->publicKey = $publicKey;
        $this->secret = $secret;
        $this->generatedAt = new \DateTime();
    }


    /**
     * Get publicKey
     *
     * @return string 
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Get generatedAt
     *
     * @return \DateTime 
     */
    public function getGeneratedAt()
    {
        return $this->generatedAt;
    }
}
