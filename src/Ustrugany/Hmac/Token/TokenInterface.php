<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\Hmac\Token;


interface TokenInterface
{
    /**
     * @return string
     */
    public function getPublicKey();

    /**
     * @return string
     */
    public function getSecret();

    /**
     * @return \DateTime
     */
    public function getGeneratedAt();
}