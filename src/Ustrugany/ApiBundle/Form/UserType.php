<?php

namespace Ranking\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email')
            ->add('plain_password', 'text', [
                'label' => 'Hasło',
                'required' => false,
            ])
            ->add('first_name', 'text', [
                'label' => 'Imię'
            ])
            ->add('last_name', 'text', [
                'label' => 'Nazwisko'
            ])
            ->add('company', 'text', [
                'label' => 'Firma'
            ])
            ->add('address', 'text', [
                'label' => 'Adres'
            ])
            ->add('program_number', 'number', [
                'label' => 'Numer w programie'
            ])
            ->add('mobile', 'text', [
                'label' => 'Tel. komórkowy'
            ])
            ->add('phone_number', 'text', [
                'label' => 'Tel. stacjonarny'
            ])

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ranking\FrontendBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ranking_user_type';
    }
}
