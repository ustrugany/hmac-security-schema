<?php

namespace Ustrugany\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Ustrugany\Hmac\Token\TokenInterface;
use Ustrugany\Hmac\Token\TokenRepositoryInterface;

class TokenRepository extends EntityRepository implements TokenRepositoryInterface
{
    /**
     * @param string $key
     * @return TokenInterface|null
     */
    public function findByKey($key)
    {
        return $this->createQueryBuilder('t')
            ->where('t.publicKey = :key')
            ->setParameter('key', $key)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
