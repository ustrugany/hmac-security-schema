<?php

namespace Ustrugany\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use Ustrugany\Hmac\Token\Token as BaseToken;

/**
 * @ORM\Table(name="token")
 * @ORM\Entity(repositoryClass="Ustrugany\ApiBundle\Repository\TokenRepository")
 * @UniqueEntity(fields={"publicKey"})
 * @UniqueEntity(fields={"secret"})
 */
class Token extends BaseToken
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="public_key", type="string", length=255, unique=true, nullable=false)
     */
    protected $publicKey;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @ORM\Column(name="secret", type="string", length=255, unique=true, nullable=false)
     */
    protected $secret;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="generated_at", type="datetime")
     */
    protected $generatedAt;

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;

    public function __construct($publicKey, $secret)
    {
        parent::__construct($publicKey, $secret);

        $this->active = false;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publicKey
     *
     * @param string $publicKey
     * @return Token
     */
    public function setPublicKey($publicKey)
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    /**
     * Get publicKey
     *
     * @return string 
     */
    public function getPublicKey()
    {
        return $this->publicKey;
    }

    /**
     * Set secret
     *
     * @param string $secret
     * @return Token
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get secret
     *
     * @return string 
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Set generatedAt
     *
     * @param \DateTime $generatedAt
     * @return Token
     */
    public function setGeneratedAt(\DateTime $generatedAt)
    {
        $this->generatedAt = $generatedAt;

        return $this;
    }

    /**
     * Get generatedAt
     *
     * @return \DateTime 
     */
    public function getGeneratedAt()
    {
        return $this->generatedAt;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }
}
