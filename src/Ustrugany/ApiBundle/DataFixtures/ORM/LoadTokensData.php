<?php

namespace Ustrugany\ApiBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

use Ustrugany\ApiBundle\Entity\Token;
use Ustrugany\Hmac\KeyGenerator\PublicSecretKeysPair\PublicSecretKeysPairGenerator;
use Ustrugany\Hmac\Token\TokenGenerator;

class LoadTokensData extends AbstractFixture implements OrderedFixtureInterface,
    ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $tokens = Yaml::parse(file_get_contents(__DIR__.DIRECTORY_SEPARATOR.'data/tokens.yml'));

        $generator = new TokenGenerator();

        foreach ($tokens as $data) {
            $token = $generator->generate();
            $token = $this->getTokenPersister()->persist($token);

            if (!$this->referenceRepository->hasReference($data['reference'])) {
                $this->referenceRepository->addReference($data['reference'], $token);
            }
        }
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }

    /**
     * @return \Ustrugany\ApiBundle\Token\TokenPersister
     */
    private function getTokenPersister()
    {
        return $this->getContainer()->get('ustrugany_api.token.persister');
    }

    /**
     * @return ContainerInterface
     */
    private function getContainer()
    {
        return $this->container;
    }
}
