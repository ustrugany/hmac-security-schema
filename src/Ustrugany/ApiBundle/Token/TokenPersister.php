<?php
/**
 * @author Piotr Strugacz <strugacz.piotr@gmail.com>
 */

namespace Ustrugany\ApiBundle\Token;
use Doctrine\ORM\EntityManager;
use Ustrugany\ApiBundle\Entity\Token;
use Ustrugany\Hmac\Token\TokenInterface;
use Ustrugany\Hmac\Token\TokenPersisterInterface;


class TokenPersister implements TokenPersisterInterface
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function persist(TokenInterface $token)
    {
        $tokenEntity = (new Token($token->getPublicKey(), $token->getSecret()))
            ->setGeneratedAt($token->getGeneratedAt());

        $this->entityManager->persist($tokenEntity);

        $this->entityManager->flush($tokenEntity);

        return $tokenEntity;
    }
}