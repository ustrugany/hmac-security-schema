<?php

namespace Ustrugany\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Ustrugany\Hmac\Signature\SignatureGenerator;

/**
 * @Route("/event")
 */
class EventController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/", name="ustrugany_api_event_get")
     * @Template(":Event:get.html.twig")
     */
    public function getAction(Request $request)
    {
        $parameters = $request->query->all();
        if ($this->get('ustrugany_api.request.validator')->validate($parameters)) {
            $this->addFlash('success', 'Request authenticated');
        } else {
            $this->addFlash('error', 'Signature does not match request');
        }

        return [
            'parameters' => $parameters,
        ];
    }
}
