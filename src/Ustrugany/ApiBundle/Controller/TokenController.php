<?php

namespace Ustrugany\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Ustrugany\ApiBundle\Entity\Token;
use Ustrugany\Hmac\Token\TokenGenerator;

/**
 * @Route("/token")
 */
class TokenController extends Controller
{
    /**
     * @Route("/", name="ustrugany_api_secure_token")
     * @Template(":Token:index.html.twig")
     * @Method({"GET"})
     */
    public function tokenAction(Request $request)
    {
        $tokens = $this->get('ustrugany_api.token.repository')->findAll();
        $deleteToken = $this->createDeleteForm();
        $createForm = $this->createCreateForm();
        $activateForm = $this->createActivateForm();

        return [
            'tokens' => $tokens,
            'deleteForm' => $deleteToken->createView(),
            'createForm' => $createForm->createView(),
            'activateForm' => $activateForm->createView(),
        ];
    }


    /**
     * @Route("/", name="ustrugany_api_secure_token_create")
     * @Method({"POST"})
     */
    public function createAction(Request $request)
    {
        $form = $this->createCreateForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $token = (new TokenGenerator())->generate();
            $this->get('ustrugany_api.token.persister')->persist($token);

            $this->addFlash('success', 'Token created');

            return $this->redirect($this->generateUrl('ustrugany_api_secure_token'));
        }

        $this->addFlash('error', 'Error while creating token');

        return $this->redirect($this->generateUrl('ustrugany_api_secure_token'));
    }


    /**
     * @Route("/{id}/activate", name="ustrugany_api_secure_token_activate")
     * @ParamConverter("token", class="UstruganyApiBundle:Token", options={"mapping": {"id": "id"}})
     * @Method({"PUT"})
     */
    public function activateAction(Request $request, Token $token)
    {
        $token->setActive(true);
        $this->getDoctrine()->getManager()->persist($token);
        $this->getDoctrine()->getManager()->flush($token);

        $this->addFlash('success', 'Token activated');

        return $this->redirect($this->generateUrl('ustrugany_api_secure_token'));
    }

    /**
     * @Route("/{id}", name="ustrugany_api_secure_token_delete")
     * @ParamConverter("token", class="UstruganyApiBundle:Token", options={"mapping": {"id": "id"}})
     * @Method({"DELETE"})
     */
    public function deleteAction(Request $request, Token $token)
    {
        $form = $this->createDeleteForm($token);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($token);
            $em->flush();

            $this->addFlash('warning', 'Token deleted');
        }

        return $this->redirect($this->generateUrl('ustrugany_api_secure_token'));
    }

    /**
     * @param Token $token|null
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm(Token $token = null)
    {
        $action = $token ? $this->generateUrl('ustrugany_api_secure_token_delete', ['id' => $token->getId()]) : null;

        return $this->createFormBuilder()
            ->setAction($action)
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function createCreateForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ustrugany_api_secure_token_create'))
            ->setMethod('POST')
            ->getForm();
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    private function createActivateForm(Token $token = null)
    {
        $action = $token ? $this->generateUrl('ustrugany_api_secure_token_activate', ['id' => $token->getId()]) : null;

        return $this->createFormBuilder()
            ->setAction($action)
            ->setMethod('PUT')
            ->getForm();
    }
}
