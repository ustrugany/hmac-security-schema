<?php

namespace Ustrugany\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\VarDumper\VarDumper;
use Ustrugany\ApiBundle\Entity\Token;
use Ustrugany\Hmac\Request\RequestFactory;
use Ustrugany\Hmac\Request\RequestSigner;
use Ustrugany\Hmac\Request\Request as HmacRequest;
/**
 * @Route("/client")
 */
class ClientController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/", name="ustrugany_api_client_index")
     * @Template(":Client:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $url = $this->generateUrl('ustrugany_api_event_get', [], UrlGeneratorInterface::ABSOLUTE_URL);

        /** @var Token $token */
        $token = current($this->get('ustrugany_api.token.repository')->findAll());
        $params = [];
        $params['query'] = 'example';
        $params['sort'] = 'example';
        $params['limit'] = 'example';
        $hmacRequest = (new RequestFactory())->create($params, $token->getPublicKey());
        (new RequestSigner())->sign($hmacRequest, $token->getSecret());
        $url .= '?' . $hmacRequest->getSignedQueryString();

        return [
            'url' => $url,
            'hmacRequest' => $hmacRequest,
        ];
    }
}
